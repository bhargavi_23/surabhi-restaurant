import './App.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

import Logincontainer from './containers/Logincontainer';
import Registercontainer from './containers/Registercontainer';
import Homecontainer from './containers/Homecontainer';
function App() {
  return (
    <div>
    <Router>
    <Switch>
				<Route exact path = '/' component = {Logincontainer} />
				<Route exact path = '/home' component = {Homecontainer} />
        <Route exact path = '/register' component = {Registercontainer} />
        </Switch>
			</Router>
    </div>
  );
}

export default App;
