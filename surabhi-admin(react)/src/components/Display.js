import React, { Component, useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
function Display(props) {
  const [data, setData] = useState([]);
  const [show, setshow] = useState(true);

  const Display = () => {
    fetch('http://localhost:1234/api/customer/showMenu').
      then(response => response.json()).
      then(data => {
        setData(data);
      });
    setshow(true);
  };

  const handleadd = (number) => {
    console.log(number);
    console.log(typeof (number));
    const data = [{
      no: number,
    }]
    const result = fetch("http://localhost:3000/user/" + newid + "/additem", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    console.log(result);
  }


  return (
    <div>
      <div class="container">
        <div class="get-quote">
          <div class="row">
            <div class="col">
            </div>
            <div >
              <h1 id="quote" style={{ textAlign: "center" }}>Restaurant Menu</h1>
            </div>
            
          </div>
        </div>
      </div>


      <br /><br /><br />
      <button class='btn-primary' style={{ marginRight: '30px', marginLeft: '340px' }} onClick={Display}>Display Veg</button>
      <br /><br />
      {show &&
        <div>
          <br /><br />
          <Table striped bordered hover variant="dark" style={{ width: '80%', margin: 'auto' }}>
            <thead>
              <tr>
                <th>Id</th>
                <th>Item</th>
                <th>Price</th>
                <th>Add to cart</th>
              </tr>
            </thead>
            {
              data.map((data, index) => {
                return (
                  <tbody>
                    <tr>
                      <td style={{ width: '20%' }}>{data.id}</td>
                      <td style={{ width: '30%' }}>{data.name}</td>
                      <td style={{ width: '20%' }}>{data.price}</td>
                      <td style={{ width: '10%' }}><button class='btn-info' onClick={handleadd.bind(this, data.no)}>Add to cart</button></td>
                    </tr>
                  </tbody>
                )
              }
              )}
          </Table>
        </div>
      }
      <br /><br />

    </div>

  );
}

export default Display;
