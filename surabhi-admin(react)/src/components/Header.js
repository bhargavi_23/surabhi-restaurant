import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { useHistory } from "react-router-dom";

function Header(props) {
  let history = useHistory();

  function handleClick() {
    history.push("/");
  }
  console.log(props);
  return (
    <div>
      <nav class="navbar navbar-dark bg-primary justify-content-center">
        <a class="navbar-brand" routerLink="/">Surabhi Restaurant (Customer)</a>
        {props.body ?

          <form class=" d-flex">
            <button class="text-white btn btn-outline-secondary float-right offset-11" type="submit" onClick={handleClick}
             >Logout</button>
          </form>


          : null}

      </nav>
    </div>
  );
}
export default Header;
