Firstly, Setup the database using Mongodb
follow the commands after opening mongo shell

1. use restaurant

//create collections

db.createCollection("customers")

db.createCollection("admins")

db.createCollection("bills")

db.createCollection("menu")


//insert into admins collection  

db.admins.insert({"_id":1,"name":"admin1","password":"pwd1"})

db.admins.insert({"_id":2,"name":"admin2","password":"pwd2"})

//insert into customers collection

db.customers.insert({"_id":"1","name":"customer1","password":"pwd1"})

db.customers.insert({"_id":"2","name":"customer2","password":"pwd2"})

db.customers.insert({"_id":"3","name":"customer3","password":"pwd3"})

db.customers.insert({"_id":"4","name":"customer4","password":"pwd4"})

db.customers.insert({"_id":"5","name":"customer5","password":"pwd5"})


//insert into menu collection

db.menu.insert({"_id":"1","name":"item1","price":"100"})

db.menu.insert({"_id":"2","name":"item2","price":"200"})

db.menu.insert({"_id":"3","name":"item3","price":"300"})

db.menu.insert({"_id":"4","name":"item4","price":"400"})

db.menu.insert({"_id":"5","name":"item5","price":"150"})


//insert into bill collection 

//better to add date of bills as today and the current month to properly test daily and monthly report

db.bill.insert({"_id":"12","itemId":"1","price":"100","date":"2021-09-06"});

db.bill.insert({"_id":"13","itemId":"2","price":"200","date":"2021-09-06"})

db.bill.insert({"_id":"14","itemId":"2","price":"200","date":"2021-09-07"})

db.bill.insert({"_id":"15","itemId":"2","price":"200","date":"2021-08-07"})



// urls to test

//customer

to login  http://localhost:1234/api/customer/login - POST - { "name" : "customer1", "password" : "pwd1" }

to logout http://localhost:1234/api/customer/logout - POST - { "name" : "customer1", "password" : "pwd1" }

to register - http://localhost:1234/api/admin/addCustomer - POST- { "name" : "customer13", "password" : "pwd13" }

to show menu - http://localhost:1234/api/customer/showMenu GET

to select items - http://localhost:1234/api/customer/selectItems/12_13 GET (items id's should be separated by '_' (underscore)


//admin routes 

login - http://localhost:1234/api/admin/login - POST - { "name" : "admin1", "password" : "pwd1" }

logout - http://localhost:1234/api/admin/logout - POST - { "name" : "admin1", "password" : "pwd1" }

to add customer - http://localhost:1234/api/admin/addCustomer - POST- { "name" : "customer13", "password" : "pwd13" }

to show all customers - http://localhost:1234/api/admin/allCustomers - GET

to updateCustomer - http://localhost:1234/api/admin/updateCustomer - PUT - {"id":"612872f3ae804b63b0b06850", "name" : "customer13", "password" : "pwd13" }

to getcustomerbyId - http://localhost:1234/api/admin/customerbyId/612872f3ae804b63b0b06850 - GET - {
    "id": "612872f3ae804b63b0b06850",
    "name": "customer13",
    "password": "pwd13",
    "loggedIn": false
}

to deletecustomerById - http://localhost:1234/api/admin/deletebyId/612872f3ae804b63b0b06850 - DELETE

to get todays Bills - http://localhost:1234/api/admin/getTodaysBills - GET

to get monthly sale - http://localhost:1234/api/admin/getMonthlySale - GET








