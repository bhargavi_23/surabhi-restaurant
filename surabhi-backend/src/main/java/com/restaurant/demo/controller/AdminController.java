package com.restaurant.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.restaurant.demo.model.Admin;
import com.restaurant.demo.model.Bill;
import com.restaurant.demo.model.Customer;
import com.restaurant.demo.model.Status;
import com.restaurant.demo.service.AdminService;
import com.restaurant.demo.service.CustomerService;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
public class AdminController {

	private final AdminService adminService;

	private final CustomerService customerService;

	@Autowired
	public AdminController(AdminService adminService, CustomerService customerService) {
		this.adminService = adminService;
		this.customerService = customerService;
	}

	// Login and Logout
	@PostMapping("/login")
	public Status loginAdmin(@RequestBody Admin admin) {
		return adminService.login(admin);
	}

	@PostMapping("/logout")
	public Status logoutAdmin(@RequestBody Admin admin) {
		adminService.logout(admin);
		return Status.SUCCESS;
	}

	// CRUD operations on Customers
	@PostMapping("/addCustomer")
	public Status addCustomer(@RequestBody Customer customer) {
		adminService.addCustomer(customer);
		return Status.SUCCESS;
	}

	@PutMapping("/updateCustomer")
	public Status updateCustomer(@RequestBody Customer customer) {
		adminService.updateCustomer(customer);
		return Status.SUCCESS;
	}

	@GetMapping("/allCustomers")
	public ResponseEntity<List<Customer>> getAllCustomers() {
		return ResponseEntity.ok(adminService.getAllCustomers());
	}

	@CrossOrigin(origins = { "http://localhost:4200", "http://localhost:3000" })
	@GetMapping("/customerbyId/{id}")
	public ResponseEntity getCustomerbyId(@PathVariable String id) {
		return ResponseEntity.ok(adminService.getCustomerById(id));
	}

	@DeleteMapping("/deletebyId/{id}")
	public Status deleteUser(@PathVariable String id) {
		return adminService.deleteCustomer(id);
	}

	@GetMapping("/getTodaysBills")
	public ResponseEntity<List<Bill>> showTodaysBills() {
		return ResponseEntity.ok(adminService.showTodaysBills());
	}

	@GetMapping("/getMonthlySale")
	public ResponseEntity<List<Bill>> showMonthlyBills() {
		return ResponseEntity.ok(adminService.totalSaleofThisMonth());
	}
}
