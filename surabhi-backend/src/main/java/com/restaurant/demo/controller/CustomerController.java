package com.restaurant.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restaurant.demo.model.Customer;
import com.restaurant.demo.model.Menu;
import com.restaurant.demo.model.Status;
import com.restaurant.demo.service.CustomerService;

@RestController
@RequestMapping("/api/customer")
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {

	private final CustomerService customerService;

	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}

	// Login and Logout
	@PostMapping("/login")
	public Status loginCustomer(@RequestBody Customer customer) {
		System.out.println("coming here");
		return customerService.login(customer);
	}

	@PostMapping("/logout")
	public Status logoutCustomer(@RequestBody Customer customer) {
		return customerService.logout(customer);
	}
	
	@PostMapping("/register")
	public Status addCustomer(@RequestBody Customer customer) {
		customerService.addCustomer(customer);
		return Status.SUCCESS;
	}
	
	@GetMapping("/showMenu")
	public ResponseEntity<List<Menu>> showMenu() {
		return ResponseEntity.ok(customerService.showMenu());
	}
	
	@GetMapping("/selectItems/{items}")
	public String selectItems(@PathVariable String items) {
		String itemsList[] = items.split("_");
		return "The total cost is : "+ Integer.toString(customerService.selectmenu(itemsList));
	}

}
