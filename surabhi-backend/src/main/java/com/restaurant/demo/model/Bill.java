package com.restaurant.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("bills")
public class Bill {

	@Id
	private String id;
	
	@Field(name = "ItemId")
	private String itemId;
	
	@Field(name = "price")
	private String price;
	
	@Field(name = "date")
	private String date;
	
	public Bill(String id, String itemId, String price, String date) {
		super();
		this.id = id;
		this.itemId = itemId;
		this.price = price;
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
}
