package com.restaurant.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.restaurant.demo.model.Customer;


@Repository
public interface CustomerRepository extends MongoRepository<Customer, String>{
	
}