package com.restaurant.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.restaurant.demo.model.Menu;

@Repository
public interface MenuRepository extends MongoRepository<Menu, String>{

}
