package com.restaurant.demo.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;  
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restaurant.demo.model.Admin;
import com.restaurant.demo.model.Bill;
import com.restaurant.demo.model.Customer;
import com.restaurant.demo.model.Status;
import com.restaurant.demo.repository.AdminRepository;
import com.restaurant.demo.repository.BillRepository;
import com.restaurant.demo.repository.CustomerRepository;

/*
 *  Contains business logic of operations that can be done by Admin
 */
@Service
public class AdminService {
	
	private final CustomerRepository customerRepository;
	
	private final AdminRepository adminRepository;
	
	private final BillRepository billRepository;
	
	@Autowired
	public AdminService(CustomerRepository customerRepository, AdminRepository adminRepository, BillRepository billRepository) {
		this.customerRepository = customerRepository;
		this.adminRepository = adminRepository;
		this.billRepository = billRepository;
	}
	
	public Status login(Admin admin) {
		List<Admin> admins = adminRepository.findAll();
        for (Admin other : admins) {
            if (other.getName().equals(admin.getName()) && other.getPassword().equals(admin.getPassword())) {
                admin.setLoggedIn(true);
                adminRepository.save(admin);
                return Status.SUCCESS;
            }
        }
        return Status.FAILURE;
	}
	
	public Status logout(Admin admin) {
		List<Admin> admins = adminRepository.findAll();
        for (Admin other : admins) {
            if (other.getName().equals(admin.getName()) && other.getPassword().equals(admin.getPassword())) {
                admin.setLoggedIn(false);
                adminRepository.save(admin);
                return Status.SUCCESS;
            }
        }
        return Status.FAILURE;
	}

	// CRUD Operations
	public void addCustomer(Customer customer) {
		customerRepository.insert(customer);
	}
	
	public void updateCustomer(Customer customer) {
		Customer savedCustomer = customerRepository.findById(customer.getId()).orElseThrow(
				() -> new RuntimeException(String.format("Cannot find Customer by Id %s", customer.getId())));

		savedCustomer.setName(customer.getName());
		savedCustomer.setPassword(customer.getPassword());
		customerRepository.save(customer);
	}
	
	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}
	
	public Customer getCustomerById(String id) {
		return customerRepository.findById(id).orElseThrow(
				() -> new RuntimeException(String.format("Cannot find Customer with Id %s", id)));
	}
	
	public Status deleteCustomer(String id) {
		customerRepository.deleteById(id);
		return Status.SUCCESS;
	}
	
	public List<Bill> showTodaysBills() {
		List<Bill> bills = billRepository.findAll();
		List<Bill> billsToday = new ArrayList<Bill>();
		
		Date date = new Date();  
	    SimpleDateFormat formatter  = new SimpleDateFormat("yyyy-MM-dd");  
	    String strDate = formatter.format(date);    
		
		for(Bill bill : bills) {
			System.out.println(bill.getDate() + "  " + strDate);
			if((bill.getDate()).equals(strDate)) {
				System.out.println("equal");
				billsToday.add(bill);
			}
		}
		
		return billsToday;
	}
	
	public List<Bill> totalSaleofThisMonth() {
		List<Bill> bills = billRepository.findAll();
		List<Bill> billsThisMonth = new ArrayList<Bill>();
		
		Date date = new Date();  
	    SimpleDateFormat formatter  = new SimpleDateFormat("yyyy-MM");  
	    String strDate = formatter.format(date);    
		
		for(Bill bill : bills) {
			System.out.println(bill.getDate() + "  " + strDate);
			if((bill.getDate().substring(0,7)).equals(strDate))
				billsThisMonth.add(bill);
		}
		return billsThisMonth;
	}

}
