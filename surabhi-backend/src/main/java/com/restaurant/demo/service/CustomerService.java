package com.restaurant.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restaurant.demo.model.Customer;
import com.restaurant.demo.model.Menu;
import com.restaurant.demo.model.Status;
import com.restaurant.demo.repository.AdminRepository;
import com.restaurant.demo.repository.CustomerRepository;
import com.restaurant.demo.repository.MenuRepository;

@Service
public class CustomerService {
	
		private final CustomerRepository customerRepository;
		
		private final AdminRepository adminRepository;
		
		private final MenuRepository menuRepository;
		
		@Autowired
		public CustomerService(CustomerRepository customerRepository, AdminRepository adminRepository, MenuRepository menuRepository) {
			this.customerRepository = customerRepository;
			this.adminRepository = adminRepository;
			this.menuRepository = menuRepository;
		}

		public Status login(Customer customer) {
			System.out.println("here in service");
			List<Customer> customers = customerRepository.findAll();
			System.out.println("here in service");
			System.out.println(customers);
	        for (Customer other : customers) {
	            if (other.equals(customer)) {
	                customer.setLoggedIn(true);
	                customerRepository.save(customer);
	                return Status.SUCCESS;
	            }
	        }
	        return Status.FAILURE;

		}
		
		public Status logout(Customer customer) {
			List<Customer> customers = customerRepository.findAll();
	        for (Customer other : customers) {
	            if (other.equals(customer)) {
	                customer.setLoggedIn(false);
	                customerRepository.save(customer);
	                return Status.SUCCESS;
	            }
	        }
	        return Status.FAILURE;
		}
		
		public void addCustomer(Customer customer) {
			customerRepository.insert(customer);
		}
		
		public List<Menu> showMenu() {
			return menuRepository.findAll();
		}
		
		public int selectmenu(String[] selectedItems) {
			int totalCost = 0;
			for(String i : selectedItems) {
				Menu menuItem = menuRepository.findById(i).orElseThrow(
						() -> new RuntimeException(String.format("Cannot find Item by Id %s",i)));;
						
				totalCost += Integer.parseInt(menuItem.getPrice());
			}
			return totalCost;
		}
}
