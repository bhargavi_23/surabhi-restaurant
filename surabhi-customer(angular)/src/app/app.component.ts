import { Component } from '@angular/core';
import { Router } from '@angular/router';
import User from './models/User';
import { AuthService } from './services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    title = 'Surabhi Restaurant Billing System - Admin';
    user!: User | null;
    response: Object = {};
    error: any = null;
    loading: boolean = false;

    constructor(private authService: AuthService, private router: Router) {
        this.authService.getCurrentUser().subscribe((data: User) => {
            this.user = data;
        });
    }


}
