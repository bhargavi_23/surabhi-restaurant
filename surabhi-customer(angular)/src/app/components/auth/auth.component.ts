import { Component, OnInit } from '@angular/core';
import User from 'src/app/models/User';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
    username: string = '';
    password: string = '';
    user!: User;
    error: any = null;
    loading: boolean = false;

    constructor(private authService: AuthService, private router: Router) {}

    ngOnInit(): void {}

    login() {
        this.loading = true;
        this.authService.login(this.username, this.password).subscribe(
            (data) => {
                this.user = data;
                this.loading = false;
                if (this.user.role === 'USER') {
                    this.error = "You're not an admin.";
                    this.username = '';
                    this.password = '';
                    return;
                }
                this.router.navigate(['/users']);
            },
            (error) => {
                this.error = error;
                console.log(error);
                this.loading = false;
            }
        );
    }
}
