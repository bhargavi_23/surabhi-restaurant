import { Component, OnInit } from '@angular/core';
import { BillsService } from 'src/app/services/bills.service';

import Bill from '../../models/Bill';

@Component({
    selector: 'app-bills',
    templateUrl: './bills.component.html',
    styleUrls: ['./bills.component.css'],
})
export class BillsComponent implements OnInit {
    loading: boolean = false;
    bills: Bill[] = [];
    error: any = null;

    constructor(private billsService: BillsService) {}

    ngOnInit(): void {
        this.loading = true;
        this.billsService.getTodaysBills().subscribe(
            (data: Bill[]) => {
                this.bills = data;
                this.loading = false;
            },
            (error) => {
                this.error = error;
                this.loading = false;
            }
        );
    }
}
