
interface Bill {
    id: string;
    itemId: string;
    price: string;
    date: string;
}

export default Bill;
