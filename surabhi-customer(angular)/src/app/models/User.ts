interface User {
    id: string;
    name: string;
    role: string;
    password: string;
}

export default User;
