import { Injectable } from '@angular/core';
import {
    HttpClient,
    HttpErrorResponse,
    HttpHeaders,
} from '@angular/common/http';
import Bill from '../models/Bill';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';
import User from '../models/User';
@Injectable({
    providedIn: 'root',
})
export class BillsService {
    user!: User;

    constructor(private http: HttpClient, private authService: AuthService) {}

    errorHandler(error: HttpErrorResponse) {
        return throwError(
            error.error.message ||
                'Something went wrong. Please Try again later.'
        );
    }

    getTodaysBills(): Observable<Array<Bill>> {
        this.authService.getCurrentUser().subscribe((data: User) => {
            this.user = data;
        });
        return this.http
            .get<Array<Bill>>('http://localhost:1234/api/admin/getTodaysBills', {
                headers: new HttpHeaders({ Authorization: `${this.user.id}` }),
            })
            .pipe(catchError(this.errorHandler));
    }
}
