import {
    HttpClient,
    HttpErrorResponse,
    HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import User from '../models/User';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root',
})
export class UsersService {
    user!: User;
    users: Array<User> = [];
    constructor(private http: HttpClient, private authService: AuthService) {}

    errorHandler(error: HttpErrorResponse) {
        return throwError(
            error.error.message ||
                'Something went wrong. Please Try again later.'
        );
    }

    getAllUsers() {
        return this.http
            .get<Array<User>>('http://localhost:1234/api/admin/allCustomers')
            .pipe(catchError(this.errorHandler));
    }

    addUser(name: string, password: string) {
        return this.http
            .post<User>('http://localhost:1234/api/admin/addCustomer ', {
                name,
                password,
            })
            .pipe(catchError(this.errorHandler));
    }

    // updateUser(username: string, userId: string) {
    //     this.authService.getCurrentUser().subscribe((data: User) => {
    //         this.user = data;
    //     });
    //     return this.http
    //         .patch(
    //             `http://localhost:1234/api/admin/customerbyId/${userId}`,
    //             {
    //                 username,
    //             },
    //             {
    //                 headers: new HttpHeaders({
    //                     Authorization: `${this.user.id}`,
                        
    //                 }),
    //             }
    //         )
    //         .pipe(catchError(this.errorHandler));
    // }

    deleteUser(userId: string) {
        this.authService.getCurrentUser().subscribe((data: User) => {
            this.user = data;
        });
        return this.http
            .delete(`http://localhost:1234/api/admin/deletebyId/${userId}`, {
                headers: new HttpHeaders({
                    Authorization: `${this.user.id}`,
                }),
            })
            .pipe(catchError(this.errorHandler));
    }
}
